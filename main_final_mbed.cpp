#include "mbed.h"

//Serial pc(USBTX, USBRX); // tx, rx

PwmOut myled1(LED1);
PwmOut myled2(LED2);
PwmOut myled3(LED3);
PwmOut myled4(LED4);

InterruptIn pb(p11);

Serial xbee(p9, p10); //Creates a variable for serial comunication through pin 9 and 10

// Joystick connections
// S-K to mbed p14 (DigitalIn)
// S-Y to mbed p15 (AnalogIn)
// S-X to mbed p16 (AnalogIn)
// VCC to board 3.3V power
// GND to board ground

DigitalIn joystick_k(p14);
AnalogIn  joystick_y(p15);
AnalogIn  joystick_x(p16);

void ReceiveXbee()  //Interrupt service routine upon receipt of an xbee byte
{
    unsigned char c = xbee.getc();  //get the byte from xbee
    //pc.putc(c);  //put the received byte to the pc terminal window
    myled1 = !myled1;  //blink an LED
}

int main() {

  //pc.printf("\n");
  //pc.printf("        WES237A - Final Project         \n");
  //pc.printf("      Autonomous Driving Assistant      \n");
  //pc.printf("            Daniel Taylor               \n");
  //pc.printf("\n");

  int count=0;

  // Use internal pullup for pushbutton
  pb.mode(PullUp);

  // Delay for initial pullup to take effect
  wait(.001);

  xbee.attach(&ReceiveXbee);  //attach the xbee interrupt service upon byte receipt
  xbee.baud(9600);  //xbee baud rate
  //pc.baud(9600);  //pc serial baud rate

  //pc.printf("\n");
  //pc.printf("Use Joystick to control iRobot\n");
  //pc.printf("\n");

  int joystick_k_value = joystick_k.read();
  float joystick_y_value = joystick_y.read();
  float joystick_x_value = joystick_x.read();

  char c_to_xbee = 'z';
  char last_c_to_xbee = 'z';

  // Send a 'z' to the Xbee as the first command
  c_to_xbee = 'z'; wait(0.5); xbee.putc('z'); wait(0.5);

  while(1) {

          joystick_k_value = joystick_k.read(); joystick_y_value = joystick_y.read(); joystick_x_value = joystick_x.read();
          wait(0.1);   //wait 0.5 secs

          //pc.printf("Count %d, Joy K %d, Joy Y %f, Joy X %f\n",count,joystick_k_value,joystick_y_value,joystick_x_value);

          if ( joystick_k_value == 0 ) {

             c_to_xbee = 'x';

             //pc.printf("Sending a %c from the mbed to the pi through Xbee\n",c_to_xbee);

          } else {

             if ( joystick_x_value >= 0.50 && joystick_x_value <= 0.75 && joystick_y_value >= 0.80 && joystick_y_value <= 1.00 ) { c_to_xbee = 'a'; }
             if ( joystick_x_value >= 0.80 && joystick_x_value <= 1.00 && joystick_y_value >= 0.80 && joystick_y_value <= 1.00 ) { c_to_xbee = 'b'; }
             if ( joystick_x_value >= 0.80 && joystick_x_value <= 1.00 && joystick_y_value >= 0.50 && joystick_y_value <= 0.75 ) { c_to_xbee = 'c'; }
             if ( joystick_x_value >= 0.80 && joystick_x_value <= 1.00 && joystick_y_value >= 0.00 && joystick_y_value <= 0.45 ) { c_to_xbee = 'd'; }
             if ( joystick_x_value >= 0.50 && joystick_x_value <= 0.75 && joystick_y_value >= 0.00 && joystick_y_value <= 0.45 ) { c_to_xbee = 'e'; }
             if ( joystick_x_value >= 0.00 && joystick_x_value <= 0.45 && joystick_y_value >= 0.00 && joystick_y_value <= 0.45 ) { c_to_xbee = 'f'; }
             if ( joystick_x_value >= 0.00 && joystick_x_value <= 0.45 && joystick_y_value >= 0.50 && joystick_y_value <= 0.75 ) { c_to_xbee = 'g'; }
             if ( joystick_x_value >= 0.00 && joystick_x_value <= 0.45 && joystick_y_value >= 0.80 && joystick_y_value <= 1.00 ) { c_to_xbee = 'h'; }
             if ( joystick_x_value >= 0.50 && joystick_x_value <= 0.75 && joystick_y_value >= 0.50 && joystick_y_value <= 0.75 ) { c_to_xbee = 'z'; }

             //pc.printf("Sending a %c from the mbed to the pi through Xbee\n",c_to_xbee);

          }

          if ( last_c_to_xbee != c_to_xbee ) {

            count++;

            //pc.printf("Sending a %c from the mbed to the pi through Xbee\n",c_to_xbee);

            if ( c_to_xbee == 'a' ) { wait(0.5); xbee.putc('a'); wait(0.5);}
            if ( c_to_xbee == 'b' ) { wait(0.5); xbee.putc('b'); wait(0.5);}
            if ( c_to_xbee == 'c' ) { wait(0.5); xbee.putc('c'); wait(0.5);}
            if ( c_to_xbee == 'd' ) { wait(0.5); xbee.putc('d'); wait(0.5);}
            if ( c_to_xbee == 'e' ) { wait(0.5); xbee.putc('e'); wait(0.5);}
            if ( c_to_xbee == 'f' ) { wait(0.5); xbee.putc('f'); wait(0.5);}
            if ( c_to_xbee == 'g' ) { wait(0.5); xbee.putc('g'); wait(0.5);}
            if ( c_to_xbee == 'h' ) { wait(0.5); xbee.putc('h'); wait(0.5);}
            if ( c_to_xbee == 'z' ) { wait(0.5); xbee.putc('z'); wait(0.5);}
            if ( c_to_xbee == 'x' ) { wait(0.5); xbee.putc('x'); wait(0.5);}

            myled2 = !myled2;  //blink an LED

          }

          wait(.005);

          last_c_to_xbee = c_to_xbee;

  }

}
