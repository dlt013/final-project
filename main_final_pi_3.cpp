#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

#include "create/create.h"

create::Create* robot;

// Globals

std::string direction = "N";

float drivespeed = 0.2;

char c_from_xbee;
char last_c_from_xbee;

// Procedures

void driveforward(float drivespeed, int diff) {

  int drivetimeperinch = 120000;

  if ( drivespeed > 0.0999 && drivespeed < 0.1001 ) {
    drivetimeperinch = 210000;
  } else if ( drivespeed > 0.1999 && drivespeed < 0.2001 ) {
    drivetimeperinch = 120000;
  } else if ( drivespeed > 0.2999 && drivespeed < 0.3001 ) {
    drivetimeperinch = 70000;
  } else {
    drivetimeperinch = 120000;
  }

  int drivetimeus = drivetimeperinch * abs(diff);

  std::cout << "diff set to " << diff << " drivetimeus set to " << drivetimeus << " drivespeed set to " << drivespeed << std::endl;

  robot->drive(drivespeed, 0.0); usleep(drivetimeus); robot->drive(0.0, 0.0);

}

void drivebackward(float drivespeed, int diff) {

}

void turnright() {

   robot->drive(0.05, -1.0); usleep(1600000);

}

void turnleft() {

   robot->drive(0.05, 1.0); usleep(1600000);

}

void drivefromto(std::string &direction, int start_x, int start_y, int end_x, int end_y) {

   std::cout << "Figure out how to get from X " << start_x << " Y " << start_y <<  " to X " << end_x << " Y " << end_y << std::endl;

   int xdiff = end_x - start_x;
   int ydiff = end_y - start_y;

   if ( direction == "N" ) {

     if ( ydiff > 0 ) { driveforward(drivespeed,ydiff); }
     if ( ydiff < 0 ) { drivebackward(drivespeed,ydiff); }

     if ( xdiff > 0 ) { turnright(); direction = "E";  driveforward(drivespeed,xdiff); }
     if ( xdiff < 0 ) { turnleft(); direction = "W"; driveforward(drivespeed,xdiff); }

   } else if ( direction == "S" ) {

     if ( ydiff > 0 ) { drivebackward(drivespeed,ydiff); }
     if ( ydiff < 0 ) { driveforward(drivespeed,ydiff); }

     if ( xdiff > 0 ) { turnleft(); direction = "E"; driveforward(drivespeed,xdiff); }
     if ( xdiff < 0 ) { turnright(); direction = "W";  driveforward(drivespeed,xdiff); }

   } else if ( direction == "E" ) {

     if ( ydiff > 0 ) { turnleft(); direction = "N"; driveforward(drivespeed,ydiff); }
     if ( ydiff < 0 ) { turnright(); direction = "S"; driveforward(drivespeed,ydiff); }

     if ( xdiff > 0 ) { driveforward(drivespeed,xdiff); }
     if ( xdiff < 0 ) { drivebackward(drivespeed,xdiff); }

   } else if ( direction == "W" ) {

     if ( ydiff > 0 ) { turnright(); direction = "N"; driveforward(drivespeed,ydiff); }
     if ( ydiff < 0 ) { turnleft(); direction = "S"; driveforward(drivespeed,ydiff); }

     if ( xdiff > 0 ) { drivebackward(drivespeed,xdiff); }
     if ( xdiff < 0 ) { driveforward(drivespeed,xdiff); }

   } else {

     std::cout << "Direction is unknown!!!" << std::endl;

   }

}

// Main Code

int main(int argc, char ** argv) {

  std::cout << std::endl;
  std::cout << "		 WES237A - Final Project	            " << std::endl;
  std::cout << "	      Autonomous Driving Assistant	            " << std::endl;
  std::cout << "		      Daniel Taylor	                    " << std::endl;
  std::cout << std::endl;

  int fd_mbed;
  int fd_xbee;
  char buf[256];
  int n;

  // Open the Port. We want read/write, no "controlling tty" status,
  // and open it no matter what state DCD is in
  //fd_mbed = open("/dev/ttyACM0", O_RDWR | O_NOCTTY | O_NDELAY);
  fd_xbee = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);

  //open mbed's USB virtual com port
  if (fd_mbed == -1) {
    perror("open_port: Unable to open /dev/ttyACM0 - ");
    return(-1);
  }

  //open xbee's USB port
  if (fd_xbee == -1) {
    perror("open_port: Unable to open /dev/ttyUSB0 - ");
    return(-1);
  }

  // Turn off blocking for reads, use (fd_xbee, F_SETFL, FNDELAY) if you want that
  fcntl(fd_xbee, F_SETFL, 0);
  //Linux Raw serial setup options
  struct termios options;
  tcgetattr(fd_xbee,&options);   //Get current serial settings in structure
  cfsetspeed(&options, B9600);   //Change a only few
  options.c_cflag &= ~CSTOPB;
  options.c_cflag |= CLOCAL;
  options.c_cflag |= CREAD;
  cfmakeraw(&options);
  tcsetattr(fd_xbee,TCSANOW,&options);    //Set serial to new settings
  sleep(1);

  ///////////////////////////////////////////////////////////
  // iRobot code
  ///////////////////////////////////////////////////////////

  std::string port = "/dev/ttyUSB1";
  int baud = 115200;
  create::RobotModel model = create::RobotModel::CREATE_2;

  if (argc > 1 && std::string(argv[1]) == "create1") {
    model = create::RobotModel::CREATE_1;
    baud = 57600;
    std::cout << "1st generation Create selected" << std::endl;
  }

  robot = new create::Create(model);

  // Attempt to connect to Create
  if (robot->connect(port, baud))
    std::cout << "Successfully connected to Create" << std::endl;
  else {
    std::cout << "Failed to connect to Create on port " << port.c_str() << std::endl;
    return 1;
  }

  // Note: Some functionality does not work as expected in Safe mode
  robot->setMode(create::MODE_FULL);

  std::cout << "battery level: " <<
    robot->getBatteryCharge() / (float) robot->getBatteryCapacity() * 100.0 << "%" << std::endl;

  bool drive = false;

  // Make sure the iRobot is at the 0,0 location

  char position_correct = '?';
  char temp_position_correct;

  std::cout << "Hello..." << std::endl;

  while(position_correct=='?') {
    std::cout << "Is your iRobot placed at the start of roadway? Yes or No (Y/N): " << std::endl;
    std::cin >> temp_position_correct;
    if ( temp_position_correct == 'Y' || temp_position_correct == 'y' ) {
      std::cout << "Okay!" <<std::endl;
      position_correct = temp_position_correct;
    } else if ( temp_position_correct == 'N' || temp_position_correct == 'n' ) {
      std::cout << "Place it at the start!" <<std::endl;
    } else {
      std::cout << "Invalid response, try again..." <<std::endl;
    }
  }

  if ( position_correct=='Y' || position_correct=='y' ) {

    temp_position_correct = 'p';
    position_correct = temp_position_correct;

  }

  //////////////////////////////////////////////////////////////////////
  // GUI Mode
  //////////////////////////////////////////////////////////////////////

  int drive_mode = 5;
  int temp_drive_mode = drive_mode;

  int number_of_coordinates;

  std::string new_grid_x;
  std::string new_grid_y;

  std::cout << "" << std::endl;
  std::cout << "Please choose from the following three modes:" << std::endl;
  std::cout << "Mode 1 : Accept a user defined route of (x,y) locations. " << std::endl;
  std::cout << "Mode 2 : Xbee Remote Control" << std::endl;
  std::cout << "Mode 3 : Xbee Remote Control + Lane Alert" << std::endl;
  std::cout << "Mode 4 : Xbee Remote Control + Snooze Control" << std::endl;
  std::cout << "" << std::endl;

  while(drive_mode >= 5) {

    std::cout << "Which mode do you want to initiate (1/2/3/4): ";
    std::cin >> temp_drive_mode;

    if ( temp_drive_mode == 1 ) {

      std::cout << "" << std::endl;
      std::cout << "Enter in the X and Y coordinates, in inches, of the path you would like to take:" << std::endl;
      std::cout << "The current location of the iRobot is assumed to be 0,0" << std::endl;
      std::cout << "" << std::endl;

      std::cout << "How many pairs of coordinates would you like to travel?" <<std::endl;
      std::cin >> number_of_coordinates;

      float iRobot_x_path[number_of_coordinates];
      float iRobot_y_path[number_of_coordinates];

      iRobot_x_path[0] = 0;
      iRobot_y_path[0] = 0;

      for (int i = 1; i <= number_of_coordinates; i++) {

        std::cout << "Enter in Coordinate " << i << ":" << std::endl;
        std::cout << "X: ";
        std::cin >> iRobot_x_path[i];
        std::cout << "Y: ";
        std::cin >> iRobot_y_path[i];

      }

      std::cout << "The iRobot will travel the following path" << std::endl;

      for (int i = 0; i <= number_of_coordinates; i++) {

        std::cout << "Point " << i <<  " : X " << iRobot_x_path[i] << " Y " << iRobot_y_path[i] << std::endl;

      }

      char ready_to_drive = '?';
      char temp_ready_to_drive;

      while(ready_to_drive=='?') {
        std::cout << "Do you want to proceed?" << std::endl;
        std::cin >> temp_ready_to_drive;
        if ( temp_ready_to_drive == 'Y' || temp_ready_to_drive == 'y' ) {
          std::cout << "Okay!" <<std::endl;
          ready_to_drive = temp_ready_to_drive;
        } else if ( temp_ready_to_drive == 'N' || temp_ready_to_drive == 'n' ) {
          std::cout << "Exiting!" <<std::endl;
        } else {
          std::cout << "Invalid response, try again..." <<std::endl;
        }
      }

      if ( temp_ready_to_drive == 'Y' || temp_ready_to_drive == 'y' ) {

        int speedinmph = 0;;
        int tempspeedinmph;

        while(speedinmph==0) {
          std::cout << "How fast do you want to drive?" << std::endl;
          std::cout << "Your choices are:" << std::endl;
          std::cout << "10 Mph (10)" << std::endl;
          std::cout << "20 Mph (20)" << std::endl;
          std::cout << "30 Mph (30)" << std::endl;
          std::cin >> tempspeedinmph;
          if ( tempspeedinmph == 10 ) {
            drivespeed = 0.1;
            std::cout << "Okay, setting your speed to 10 Mph!" <<std::endl;
            speedinmph = tempspeedinmph;
          } else if ( tempspeedinmph == 20 ) {
            drivespeed = 0.2;
            std::cout << "Okay, setting your speed to 20 Mph!" <<std::endl;
            speedinmph = tempspeedinmph;
          } else if ( tempspeedinmph == 30 ) {
            drivespeed = 0.3;
            std::cout << "Okay, setting your speed to 30 Mph!" <<std::endl;
            speedinmph = tempspeedinmph;
          } else {
            std::cout << "Invalid response, try again..." <<std::endl;
          }
        }

        for (int i = 1; i <= number_of_coordinates; i++) {

          drivefromto(direction,iRobot_x_path[i-1],iRobot_y_path[i-1],iRobot_x_path[i],iRobot_y_path[i]);

        }

      }

      drive_mode = temp_drive_mode;

    } else if ( temp_drive_mode == 2 ) {

      c_from_xbee = 'z';
      last_c_from_xbee = 'z';

      std::cout << "Making it here 1, c_from_xbee : " << c_from_xbee << " last_c_from_xbee : " << last_c_from_xbee <<std::endl;

      while (last_c_from_xbee != 'x') {
        sleep(1);               //one second delay
        read(fd_xbee,buf,1);    //read echo character
        printf("%s\n\r",buf);   //print in terminal window
	if ( buf[0] == 'x' ) {
	  c_from_xbee = 'x'; robot->drive(0.0, 0.0);
	} else if ( buf[0] == 'a' ) {
	  c_from_xbee = 'a'; robot->drive(0.2, 0.0);
	} else if ( buf[0] == 'b' ) {
	  c_from_xbee = 'b'; robot->drive(0.1, -0.5);
	} else if ( buf[0] == 'c' ) {
	  c_from_xbee = 'c'; robot->drive(0.05, -1.0);
	} else if ( buf[0] == 'd' ) {
	  c_from_xbee = 'd'; robot->drive(-0.1, 0.5);
	} else if ( buf[0] == 'e' ) {
	  c_from_xbee = 'e'; robot->drive(-0.1, 0.0);
	} else if ( buf[0] == 'f' ) {
	  c_from_xbee = 'f'; robot->drive(-0.1, -0.5);
	} else if ( buf[0] == 'g' ) {
	  c_from_xbee = 'g'; robot->drive(0.05, 1.0);
	} else if ( buf[0] == 'h' ) {
	  c_from_xbee = 'h'; robot->drive(0.1, 0.5);
	} else if ( buf[0] == 'z' ) {
	  c_from_xbee = 'z'; robot->drive(0.0, 0.0);
	} else {
	  c_from_xbee = last_c_from_xbee;
	}
        if (last_c_from_xbee != c_from_xbee) {
          std::cout << "Making it here 2, c_from_xbee : " << c_from_xbee << " last_c_from_xbee : " << last_c_from_xbee <<std::endl;
	}
        last_c_from_xbee = c_from_xbee;
      }

      drive_mode = temp_drive_mode;

    } else if ( temp_drive_mode == 3 ) {

      drive_mode = temp_drive_mode;

    } else if ( temp_drive_mode == 4 ) {

      drive_mode = temp_drive_mode;

    } else {

       std::cout << "Invalid response, try again..." <<std::endl;

    }

  }

  std::cout << "Exiting..." <<  std::endl;

  //////////////////////////////////////////////////////////////////////
  // Don't forget to clean up and close the mbed port
  //////////////////////////////////////////////////////////////////////

  tcdrain(fd_mbed);
  close(fd_mbed);

  tcdrain(fd_xbee);
  close(fd_xbee);

  //////////////////////////////////////////////////////////////////////
  // Clean up the iRobot port
  //////////////////////////////////////////////////////////////////////

  std::cout << "Stopping Create." << std::endl;

  // Turn off lights
  robot->setPowerLED(0, 0);
  robot->enableDebrisLED(false);
  robot->enableCheckRobotLED(false);
  robot->setDigitsASCII(' ', ' ', ' ', ' ');

  // Make sure to disconnect to clean up
  robot->disconnect();
  delete robot;

  return 0;
}
